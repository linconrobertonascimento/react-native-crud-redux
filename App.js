import AppRouter from './src/Router';
import { PaperProvider } from 'react-native-paper';

export default function App() {
  return (
    <PaperProvider>
      <AppRouter />
    </PaperProvider>
  )
}

