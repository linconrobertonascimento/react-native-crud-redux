import React, { useReducer, useState } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { Button, DataTable, TextInput } from 'react-native-paper';
import { vehicleReducer } from '../reducers/VehicleReducer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function VehicleScreen() {
    const [state, dispatch] = useReducer(vehicleReducer, []);
    const [vehicleId, setVehicleId] = useState(null);
    const [nomeVeiculo, setNomeVeiculo] = useState('');
    const [cor, setCor] = useState('');
    const [placa, setPlaca] = useState('');
    const [editing, setEditing] = useState(false);

    const handleUpdate = () => {
        dispatch({ type: 'UPDATE', payload: { id: vehicleId, nomeVeiculo, cor, placa } });
        setEditing(false);
        setNomeVeiculo('');
        setCor('');
        setPlaca('');
        setVehicleId(null);
    };


    const handleEdit = (item) => {
        setNomeVeiculo(item.nomeVeiculo);
        setCor(item.cor);
        setPlaca(item.placa);
        setVehicleId(item.id);
        setEditing(true);
    };

    return (
        <View style={styles.container}>
            <TextInput
                label="Nome do veículo"
                value={nomeVeiculo}
                onChangeText={setNomeVeiculo}
                mode="outlined"
                style={styles.input}
            />
            <TextInput
                label="Cor"
                value={cor}
                onChangeText={setCor}
                mode="outlined"
                style={styles.input}
            />
            <TextInput
                label="Placa"
                value={placa}
                onChangeText={setPlaca}
                mode="outlined"
                style={styles.input}
            />

            {editing ? (
                <Button mode="contained" onPress={handleUpdate}>
                    Atualizar
                </Button>
            ) : (
                <Button mode="contained" onPress={() => {
                    dispatch({ type: 'ADD', payload: { nomeVeiculo, cor, placa } });
                    setNomeVeiculo('');
                    setCor('');
                    setPlaca('');
                }}>
                    Adicionar
                </Button>
            )}

            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>ID</DataTable.Title>
                    <DataTable.Title>Nome</DataTable.Title>
                    <DataTable.Title>Cor</DataTable.Title>
                    <DataTable.Title>Placa</DataTable.Title>
                    <DataTable.Title>Ações</DataTable.Title>
                </DataTable.Header>

                <FlatList
                    data={state}
                    keyExtractor={item => item.placa}
                    renderItem={({ item }) => (
                        <DataTable.Row>
                            <DataTable.Cell>{item.id}</DataTable.Cell>
                            <DataTable.Cell>{item.nomeVeiculo}</DataTable.Cell>
                            <DataTable.Cell>{item.cor}</DataTable.Cell>
                            <DataTable.Cell>{item.placa}</DataTable.Cell>
                            <DataTable.Cell>
                                <View style={styles.viewButton}>
                                    <Icon name="pencil" onPress={() => handleEdit(item)} size={20} color="blue" />
                                    <Icon name="close" onPress={() => dispatch({ type: 'REMOVE', payload: item.id })}
                                        size={20} color="red" />
                                </View>
                            </DataTable.Cell>
                        </DataTable.Row>
                    )}
                />
            </DataTable>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        justifyContent: 'center',
    },
    viewButton: {
        display: 'flex',
        flexDirection: 'row'
    },
    input: {
        marginBottom: 10,
    },
    buttonEdit: {
        backgroundColor: '#535fee'
    },
    buttonDelete: {
        backgroundColor: '#ef3f3f'
    }
});