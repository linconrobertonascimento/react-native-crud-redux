import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import VehicleScreen from './components/VehicleScreen';

const Stack = createStackNavigator();

function AppRouter() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Veículos REDUX">
                <Stack.Screen name="Veículos REDUX" component={VehicleScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default AppRouter;
