export function vehicleReducer(state, action) {
    switch (action.type) {
        case 'ADD':
            const newId = state.length + 1;
            return [...state, { ...action.payload, id: newId }];
        case 'REMOVE':
            return state.filter(vehicle => vehicle.id !== action.payload);
        case 'UPDATE':
            return state.map(vehicle => vehicle.id === action.payload.id ? action.payload : vehicle);
        case 'READ':
            return state;
        default:
            return state;
    }
}
