Iniciando o projeto.

Certifique de ter instalado o `node` e o `expo`.

Logo após, rode um `npm i` na pasta do projeto, para baixar as dependências.

Para iniciar o projeto, rode `npm run android`, com o emulador aberto.